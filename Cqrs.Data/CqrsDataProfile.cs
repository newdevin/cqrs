﻿using AutoMapper;
using Cqrs.Domain;

namespace Cqrs.Data
{
    public class CqrsDataProfile: Profile
    {

        public CqrsDataProfile()
        {
            CreateMap<Product, ProductEntity>().ReverseMap();
        }

    }
}
