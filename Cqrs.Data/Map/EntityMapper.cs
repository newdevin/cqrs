﻿using AutoMapper;
using System.Collections.Generic;

namespace Cqrs.Data.Map
{
    public class EntityMapper : IEntityMapper
    {
        private readonly IMapper mapper;

        public EntityMapper(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public T Map<T>(object obj)
        {
            return mapper.Map<T>(obj);
        }

        public IEnumerable<T> Map<T>(IEnumerable<object> obj)
        {
            return mapper.Map<IEnumerable<T>>(obj);
        }
    }
}
