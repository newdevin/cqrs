﻿using System.Collections.Generic;

namespace Cqrs.Data.Map
{
    public interface IEntityMapper
    {
        T Map<T>(object obj);
        IEnumerable<T> Map<T>(IEnumerable<object> obj);
    }
}
