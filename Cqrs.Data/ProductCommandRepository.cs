﻿using Cqrs.Data.Map;
using Cqrs.Domain;
using Cqrs.Service.DataService;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cqrs.Data
{
    public class ProductCommandRepository : IProductCommandDataService
    {
        readonly ProductDbContext productUnitOfWork;
        readonly IEntityMapper entityMapper;

        public ProductCommandRepository(ProductDbContext productUnitOfWork, IEntityMapper entityMapper)
        {
            this.entityMapper = entityMapper;
            this.productUnitOfWork = productUnitOfWork;
        }

        public async Task AddAsync(Product product)
        {
            ProductEntity entity = entityMapper.Map<ProductEntity>(product);
            productUnitOfWork.Products.Add(entity);
            await productUnitOfWork.SaveChangesAsync();
        }

       
    }
}
