﻿using Microsoft.EntityFrameworkCore;

namespace Cqrs.Data
{
    public class ProductDbContext : DbContext
    {
        
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        { }
        public DbSet<ProductEntity> Products { get; set; }

    }
}
