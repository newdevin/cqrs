﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Cqrs.Data
{
    [Table("Product")]
    public class ProductEntity
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}