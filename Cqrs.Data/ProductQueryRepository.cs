﻿using Cqrs.Domain;
using Cqrs.Service.DataService;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Cqrs.Data
{
    public class ProductQueryRepository : IProductQueryDataService
    {
        readonly string connectionString;

        public ProductQueryRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public async Task<Product> GetAsync(int id)
        {
            string query = "Select Id, Code,Name from Product Where code = @id";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QuerySingleOrDefaultAsync<Product>(query, new { id });
            }

        }

        public async Task<Product> GetByCodeAsync(int code)
        {
            string query = "Select Id, Code,Name from Product Where code = @code";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QuerySingleOrDefaultAsync<Product>(query, new { code });
            }
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            string query = "Select Id, Code,Name from Product";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return await connection.QueryAsync<Product>(query);
            }
        }
    }
}
