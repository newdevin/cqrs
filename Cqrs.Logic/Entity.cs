﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cqrs.Domain
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
