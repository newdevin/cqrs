﻿using Cqrs.Domain;
using System;

namespace Cqrs.Domain
{
    public class Product : Entity
    {
        public int Code { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Product;
            if (other == null)
                return false;

            return this.Code.Equals(obj);

        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Code);
        }
    }
}
