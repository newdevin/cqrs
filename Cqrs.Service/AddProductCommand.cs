﻿using Cqrs.Domain;
using Cqrs.Service.DataService;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Cqrs.Service
{
    public sealed class AddProductCommand : IRequest
    {
        public readonly int Code;
        public readonly string Name;

        public AddProductCommand(int code, string name)
        {
            Name = name;
            Code = code;
        }

    }

    public sealed class AddStudentCommandHandler : AsyncRequestHandler<AddProductCommand>
    {
        readonly IProductCommandDataService productCommandDataService;
        readonly IProductQueryDataService productQueryDataService;
        readonly IUnitOfWork unitOfWork;

        public AddStudentCommandHandler(IUnitOfWork unitOfWork, IProductCommandDataService productCommandDataService, 
            IProductQueryDataService productQueryDataService)
        {
            this.unitOfWork = unitOfWork;
            this.productQueryDataService = productQueryDataService;
            this.productCommandDataService = productCommandDataService;
        }

        protected override async Task Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            var existingProduct = await productQueryDataService.GetByCodeAsync(request.Code);
            if ( existingProduct != null)
                throw new ExistsException($"A product with code {request.Code} already exists");

            var product = new Product() { Code = request.Code, Name = request.Name };
            await productCommandDataService.AddAsync(product);
            await unitOfWork.SaveAsync();
        }

        
    }
}
