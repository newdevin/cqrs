﻿using Cqrs.Domain;
using System.Threading.Tasks;

namespace Cqrs.Service.DataService
{
    public interface IProductCommandDataService
    {
        Task AddAsync(Product product);   
    }
}
