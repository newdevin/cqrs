﻿using Cqrs.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cqrs.Service.DataService
{
    public interface IProductQueryDataService
    {
        Task<Product> GetByCodeAsync(int code);
        Task<Product> GetAsync(int code);
        Task<IEnumerable<Product>> GetProducts();
    }
}
