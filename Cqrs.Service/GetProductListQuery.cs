﻿using Cqrs.Domain;
using Cqrs.Service.DataService;
using Dapper;
using MediatR;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Cqrs.Service
{
    public class GetProductListQuery : IRequest<IEnumerable<Product>>
    {

    }

    public class GetStudentListQueryHandler : IRequestHandler<GetProductListQuery, IEnumerable<Product>>
    {
        readonly IProductQueryDataService productQueryDataService;

        public GetStudentListQueryHandler(IProductQueryDataService productQueryDataService)
        {
            this.productQueryDataService = productQueryDataService;
        }

        public async Task<IEnumerable<Product>> Handle(GetProductListQuery request, CancellationToken cancellationToken)
        {
            return await productQueryDataService.GetProducts();
        }

        
    }
}
