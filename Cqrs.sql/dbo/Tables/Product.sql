﻿CREATE TABLE [dbo].[Product] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Code] INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL
);

