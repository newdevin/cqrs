﻿using AutoMapper;
using Cqrs.Data;
using Cqrs.Data.Map;
using Cqrs.Domain;
using Cqrs.Service;
using Cqrs.Service.DataService;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Cqrs
{
    class Program
    {
        public static IConfiguration configuration;

        static void Main(string[] args)
        {
            Initialize();

            configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            ServiceProvider provider = services.BuildServiceProvider();

            IMediator mediator = provider.GetService<IMediator>();

            Unit result = mediator.Send(new AddProductCommand(2, "Dvd Player")).Result;

            IEnumerable<Product> products = mediator.Send(new GetProductListQuery()).Result;

            foreach (Product product in products)
            {
                Console.WriteLine($"Id:{product.Id}, Code:{product.Code}, Name: {product.Name}");
            }

            Console.ReadLine();

        }

        static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IProductCommandDataService, ProductCommandRepository>();
            services.AddTransient<IProductQueryDataService, ProductQueryRepository>(x =>
            {
                return new ProductQueryRepository(configuration.GetConnectionString("Cqrs"));
            });
            services.AddMediatR(typeof(AddProductCommand).Assembly);
            services.AddEntityFrameworkSqlServer();
            services.AddDbContext<ProductDbContext>(opt =>
            {
                opt.UseSqlServer(configuration.GetConnectionString("Cqrs"));
            });
            services.AddTransient<IEntityMapper, EntityMapper>();
            services.AddSingleton<IMapper>(Mapper.Instance);
            services.AddTransient<IRequestHandler<GetProductListQuery, IEnumerable<Product>>>((x) =>
           {
               return new GetStudentListQueryHandler(x.GetRequiredService<IProductQueryDataService>());
           });
            services.AddTransient<IRequestHandler<AddProductCommand, Unit>>((x) =>
            {
                return new AddStudentCommandHandler(new ProductUnitOfWork(x.GetService<ProductDbContext>()), x.GetService<IProductCommandDataService>(), x.GetRequiredService<IProductQueryDataService>());
            });

        }

        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CqrsDataProfile>();
            });
        }
    }
}
